All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.0] - 2020-09-12
### Added
- Initial version GCP database

### Changed
- Refactor name product

## [1.0.1] - 2020-08-28
### Added
- Added gcp_credentials as a secret

## [1.0.0] - 2020-08-09
### Added
- Initial version newrelic product with conditions for K8s
- Initial version RDS product with SG
- Initial version EC2 product with SG
- Initial version GKE product

