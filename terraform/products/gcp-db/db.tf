variable "zone" {
  description = "zone"
  type        = string
}

variable "machine_type" {
  description = "machine type"
  type        = string
}

variable "authorized_networks" {
  default     = []
  type        = list(map(string))
  description = "List of mapped public networks authorized to access to the instances. Default - short range of GCP health-checkers IPs"
}

module "postgresql-db" {
  source               = "../../vendor/terraform-google-sql-db/modules/postgresql"
  name                 = var.name
  random_instance_name = true
  database_version     = "POSTGRES_9_6"
  project_id           = var.project_id
  zone                 = var.zone
  region               = var.gcp_region
  tier                 = var.machine_type
  user_name            = "admin"
  user_password        = "admin"

  ip_configuration = {
    ipv4_enabled        = true
    private_network     = null
    require_ssl         = false
    authorized_networks = var.authorized_networks
  }
}

output "public_ip_address" {
  value = module.postgresql-db.public_ip_address
}

output "instance_connection_name" {
  value = module.postgresql-db.instance_connection_name
}
