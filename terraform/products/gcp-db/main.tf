terraform {
  backend "s3" {}
  required_version = ">= 0.12.24"

  required_providers {
    aws      = ">= 2.55.0"
    local    = ">= 1.2"
    template = ">= 2.1"
  }
}

provider "aws" {
  region = var.aws_region
}

provider "google" {
  credentials = var.gcp_credentials
  project     = var.project_id
  region      = var.gcp_region
}

provider "google-beta" {
  credentials = var.gcp_credentials
  project     = var.project_id
  region      = var.gcp_region
}

variable "aws_region" {
  description = "AWS Region"
  type        = string
}

variable "gcp_region" {
  description = "GCP Region"
  type        = string
}

variable "gcp_credentials" {
  description = "GCP credentials"
  type        = string
}

variable "name" {
  description = "name"
  type        = string
}

variable "project_id" {
  description = "project id"
  type        = string
}
