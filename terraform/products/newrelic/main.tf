terraform {
  backend "s3" {}
  required_version = ">= 0.12.24"

  required_providers {
    aws      = ">= 2.55.0"
    local    = ">= 1.2"
    template = ">= 2.1"
    newrelic = "~> 2.0"
  }
}

provider "aws" {
  region = var.aws_region
}

variable "environment" {
  description = "environment"
  type        = string
}

variable "aws_region" {
  description = "AWS Region"
  type        = string
}

variable "account_id" {
  description = "account id"
  type        = string
}

variable "api_key" {
  description = "api key"
  type        = string
}

variable "admin_api_key" {
  description = "admin api key"
  type        = string
}

provider "newrelic" {
  api_key       = var.api_key
  admin_api_key = var.admin_api_key
  account_id    = var.account_id
}
