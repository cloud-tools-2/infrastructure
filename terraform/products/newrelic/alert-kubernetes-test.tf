resource "newrelic_alert_policy" "k8s_alert_test" {
  name                = "K8sTest"
  incident_preference = "PER_CONDITION"
  account_id          = var.account_id
}


resource "newrelic_nrql_alert_condition" "kubernetes_alert_condition_without_where" {
  policy_id  = newrelic_alert_policy.k8s_alert_test.id
  account_id = var.account_id

  name                 = "Without where"
  type                 = "static"
  enabled              = true
  value_function       = "single_value"
  violation_time_limit = "TWENTY_FOUR_HOURS"

  nrql {
    query             = <<NRQL
SELECT 2
FROM K8sContainerSample
FACET entityName
NRQL
    evaluation_offset = 3
  }

  critical {
    operator              = "above"
    threshold             = 1
    threshold_duration    = 120
    threshold_occurrences = "ALL"
  }
}

resource "newrelic_nrql_alert_condition" "kubernetes_alert_condition_with_where" {
  policy_id  = newrelic_alert_policy.k8s_alert_test.id
  account_id = var.account_id

  name                 = "WITH WHERE"
  type                 = "static"
  enabled              = true
  value_function       = "single_value"
  violation_time_limit = "TWENTY_FOUR_HOURS"

  nrql {
    query       = <<NRQL
SELECT 2
FROM K8sContainerSample
WHERE `label.app` != 'hello-kubernetes'
FACET entityName
NRQL
    evaluation_offset = 3
  }

  critical {
    operator              = "above"
    threshold             = 1
    threshold_duration    = 120
    threshold_occurrences = "ALL"
  }
}

resource "newrelic_nrql_alert_condition" "kubernetes_alert_condition_only_with_label" {
  policy_id  = newrelic_alert_policy.k8s_alert_test.id
  account_id = var.account_id

  name                 = "ONLY WITH LABEL"
  type                 = "static"
  enabled              = true
  value_function       = "single_value"
  violation_time_limit = "TWENTY_FOUR_HOURS"

  nrql {
    query       = <<NRQL
SELECT 2
FROM K8sContainerSample
WHERE `label.app` = 'hello-kubernetes'
FACET entityName
NRQL
    evaluation_offset = 3
  }

  critical {
    operator              = "above"
    threshold             = 1
    threshold_duration    = 120
    threshold_occurrences = "ALL"
  }
}


resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_only_label" {
  policy_id  = newrelic_alert_policy.k8s_alert_test.id

  name       = "ONLY LABEL"
  type       = "infra_metric"
  event      = "K8sContainerSample"
  select     = "1"
  comparison = "equal"
  where      = "`label.app` = 'hello-kubernetes'"

  critical {
    duration      = 1
    value         = 1
    time_function = "all"
  }
}
