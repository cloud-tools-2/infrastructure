module "kubernetes" {
  source     = "./conditions-kubernetes"
  env        = var.environment
  account_id = var.account_id
}
