resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_deployment_mismatch_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Deployment has not matched the expected number of replicas"
  type       = "infra_metric"
  event      = "K8sDeploymentSample"
  select     = "podsDesired - podsAvailable"
  comparison = "above"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_deployment_mismatch" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Deployment has not matched the expected number of replicas"
  type       = "infra_metric"
  event      = "K8sDeploymentSample"
  select     = "podsDesired - podsAvailable"
  comparison = "above"

  critical {
    duration      = 30
    value         = 0
    time_function = "all"
  }
}