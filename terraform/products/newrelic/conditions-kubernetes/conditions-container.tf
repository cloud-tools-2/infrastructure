resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_container_cpu_limit_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Container CPU usage is over limit"
  type       = "infra_metric"
  event      = "K8sContainerSample"
  select     = "(cpuUsedCores/cpuLimitCores)*100"
  comparison = "above"

  critical {
    duration      = 30
    value         = 100
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_container_cpu_limit" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Container CPU usage is reaching/over limit"
  type       = "infra_metric"
  event      = "K8sContainerSample"
  select     = "(cpuUsedCores/cpuLimitCores)*100"
  comparison = "above"

  critical {
    duration      = 5
    value         = 95
    time_function = "all"
  }

  warning {
    duration      = 5
    value         = 90
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_container_cpu_request" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Container CPU usage is over requested"
  type       = "infra_metric"
  event      = "K8sContainerSample"
  select     = "(cpuUsedCores/cpuRequestedCores)*100"
  comparison = "above"

  critical {
    duration      = 15
    value         = 150
    time_function = "all"
  }

  warning {
    duration      = 15
    value         = 100
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_container_memory_limit" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Container memory usage is close to the limit"
  type       = "infra_metric"
  event      = "K8sContainerSample"
  select     = "(memoryWorkingSetBytes/memoryLimitBytes)*100"
  comparison = "above"

  critical {
    duration      = 5
    value         = 95
    time_function = "all"
  }

  warning {
    duration      = 5
    value         = 85
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_container_memory_request" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Container memory usage is over requested"
  type       = "infra_metric"
  event      = "K8sContainerSample"
  select     = "(memoryWorkingSetBytes/memoryRequestedBytes)*100"
  comparison = "above"

  critical {
    duration      = 15
    value         = 100
    time_function = "all"
  }

  warning {
    duration      = 15
    value         = 150
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_container_out_of_space" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Container is running out of space"
  type       = "infra_metric"
  event      = "K8sContainerSample"
  select     = "fsUsedPercent"
  comparison = "above"

  critical {
    duration      = 5
    value         = 90
    time_function = "all"
  }

  warning {
    duration      = 5
    value         = 75
    time_function = "all"
  }
}

resource "newrelic_nrql_alert_condition" "kubernetes_alert_condition_container_crash_looping_crit" {
  policy_id  = newrelic_alert_policy.kubernetes_critical.id
  account_id = var.account_id

  name                 = "Container is restarting too many times"
  type                 = "static"
  enabled              = true
  value_function       = "single_value"
  violation_time_limit = "TWENTY_FOUR_HOURS"

  nrql {
    query             = <<NRQL
SELECT (max(restartCount) - min(restartCount))
FROM K8sContainerSample
FACET entityName
NRQL
    evaluation_offset = 3
  }

  critical {
    operator              = "above"
    threshold             = 1
    threshold_duration    = 900
    threshold_occurrences = "ALL"
  }
}
