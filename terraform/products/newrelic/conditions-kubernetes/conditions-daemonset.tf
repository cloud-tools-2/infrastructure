resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_daemon_set_rollout_stuck_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Only part of the Pods of DaemonSet are scheduled and ready"
  type       = "infra_metric"
  event      = "K8sDaemonsetSample"
  select     = "podsDesired - podsReady"
  comparison = "above"

  critical {
    duration      = 30
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_daemon_set_rollout_stuck" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Only part of the Pods of DaemonSet are scheduled and ready"
  type       = "infra_metric"
  event      = "K8sDaemonsetSample"
  select     = "podsDesired - podsReady"
  comparison = "above"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_daemon_set_mis_scheduled" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Pods of DaemonSet are running where they are not supposed to run"
  type       = "infra_metric"
  event      = "K8sDaemonsetSample"
  select     = "podsMisscheduled"
  comparison = "above"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_daemon_set_not_scheduled_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Pods of DaemonSet are not scheduled"
  type       = "infra_metric"
  event      = "K8sDaemonsetSample"
  select     = "podsDesired - podsScheduled"
  comparison = "above"

  critical {
    duration      = 30
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_daemon_set_not_scheduled" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Pods of DaemonSet are not scheduled"
  type       = "infra_metric"
  event      = "K8sDaemonsetSample"
  select     = "podsDesired - podsScheduled"
  comparison = "above"

  critical {
    duration      = 10
    value         = 0
    time_function = "all"
  }
}
