resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_node_space_left_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Filesystem space low"
  type       = "infra_metric"
  event      = "K8sNodeSample"
  select     = "(fsUsedBytes/fsCapacityBytes)*100"
  comparison = "above"

  critical {
    duration      = 60
    value         = 95
    time_function = "all"
  }

  warning {
    duration      = 60
    value         = 90
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_node_inodes_left_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Filesystem is low on inodes"
  type       = "infra_metric"
  event      = "K8sNodeSample"
  select     = "(fsInodesUsed/fsInodes)*100"
  comparison = "above"

  critical {
    duration      = 60
    value         = 97
    time_function = "all"
  }

  warning {
    duration      = 60
    value         = 95
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_node_network_interface_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Network interface is reporting many receive/transmit errors"
  type       = "infra_metric"
  event      = "K8sNodeSample"
  select     = "net.errorsPerSecond"
  comparison = "above"

  critical {
    duration      = 60
    value         = 10
    time_function = "all"
  }
}

resource "newrelic_nrql_alert_condition" "kubernetes_alert_condition_node_many_pods" {
  policy_id  = newrelic_alert_policy.kubernetes_non_critical.id
  account_id = var.account_id

  name                 = "Node contains too many pods"
  type                 = "static"
  enabled              = true
  value_function       = "single_value"
  violation_time_limit = "TWENTY_FOUR_HOURS"

  nrql {
    query             = <<NRQL
SELECT filter(uniqueCount(podName), WHERE isScheduled = 1)/latest(allocatablePods)
FROM K8sPodSample, K8sNodeSample
FACET nodeName
NRQL
    evaluation_offset = 3
  }

  critical {
    operator              = "above"
    threshold             = 0.95
    threshold_duration    = 900
    threshold_occurrences = "ALL"
  }
}
