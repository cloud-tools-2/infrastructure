resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_volume_filling_up_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Persistent volume usage is too high"
  type       = "infra_metric"
  event      = "K8sVolumeSample"
  select     = "(fsUsedBytes/fsCapacityBytes)*100"
  comparison = "above"
  where      = "(`persistent` = 'true')"

  critical {
    duration      = 5
    value         = 97
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_volume_filling_up" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Persistent volume usage is too high"
  type       = "infra_metric"
  event      = "K8sVolumeSample"
  select     = "(fsUsedBytes/fsCapacityBytes)*100"
  comparison = "above"
  where      = "(`persistent` = 'true')"

  critical {
    duration      = 10
    value         = 90
    time_function = "all"
  }

  warning {
    duration      = 60
    value         = 85
    time_function = "all"
  }
}