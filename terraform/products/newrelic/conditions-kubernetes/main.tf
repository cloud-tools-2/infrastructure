variable "env" {
  description = "dev/nonprod/prod"
  type        = string
}

variable "account_id" {
  description = "account id"
  type        = string
}

resource "newrelic_alert_policy" "kubernetes_critical" {
  name                = "kubernetes-${var.env}-critical"
  incident_preference = "PER_CONDITION_AND_TARGET"
  account_id          = var.account_id
}

resource "newrelic_alert_policy" "kubernetes_non_critical" {
  name                = "kubernetes-${var.env}-warning"
  incident_preference = "PER_CONDITION_AND_TARGET"
  account_id          = var.account_id
}

output "newrelic_alert_policy_critical_id" {
  value       = newrelic_alert_policy.kubernetes_critical.id
  description = "Critical alert policy id"
}

output "newrelic_alert_policy_non_critical_id" {
  value       = newrelic_alert_policy.kubernetes_non_critical.id
  description = "Non-critical alert policy id"
}
