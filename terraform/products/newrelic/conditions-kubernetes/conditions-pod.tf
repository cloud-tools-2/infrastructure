resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_pod_not_ready_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Pod is not ready"
  type       = "infra_metric"
  event      = "K8sPodSample"
  select     = "isReady"
  comparison = "equal"
  where      = "(`status` != 'Succeeded') AND (`status` != 'Failed')"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_pod_not_ready" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "Pod is not ready"
  type       = "infra_metric"
  event      = "K8sPodSample"
  select     = "isReady"
  comparison = "equal"
  where      = "(`status` != 'Succeeded') AND (`status` != 'Failed')"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}


resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_pod_unable_scheduled_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Pod was unable to be scheduled"
  type       = "infra_metric"
  event      = "K8sPodSample"
  select     = "isScheduled"
  comparison = "equal"

  critical {
    duration      = 10
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_pod_pending_state_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Pod has been in pending state for longer than 1 hour"
  type       = "infra_metric"
  event      = "K8sPodSample"
  select     = "isReady"
  comparison = "equal"
  where      = "(`status` = 'Pending')"

  critical {
    duration      = 60
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_pod_job_failed_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "Job failed to complete"
  type       = "infra_metric"
  event      = "K8sPodSample"
  select     = "isReady"
  comparison = "equal"
  where      = "(`createdKind` = 'Job') AND (`status` = 'Failed')"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}
