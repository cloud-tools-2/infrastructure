resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_replica_set_mismatch_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "ReplicaSet doesn't have desired amount of pods"
  type       = "infra_metric"
  event      = "K8sReplicasetSample"
  select     = "podsDesired - podsReady"
  comparison = "above"

  critical {
    duration      = 30
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_replica_set_mismatch" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "ReplicaSet doesn't have desired amount of pods"
  type       = "infra_metric"
  event      = "K8sReplicasetSample"
  select     = "podsDesired - podsReady"
  comparison = "above"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}