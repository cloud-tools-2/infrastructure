resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_stateful_set_replicas_mismatch_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "StatefulSet has not matched the expected number of replicas"
  type       = "infra_metric"
  event      = "K8sStatefulsetSample"
  select     = "podsDesired - podsReady"
  comparison = "above"

  critical {
    duration      = 30
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_stateful_set_replicas_mismatch" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "StatefulSet has not matched the expected number of replicas"
  type       = "infra_metric"
  event      = "K8sStatefulsetSample"
  select     = "podsDesired - podsReady"
  comparison = "above"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_stateful_set_generation_mistmatch_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "StatefulSet generation does not match"
  type       = "infra_metric"
  event      = "K8sStatefulsetSample"
  select     = "observedGeneration - metadataGeneration"
  comparison = "above"

  critical {
    duration      = 30
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_stateful_set_generation_mistmatch" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "StatefulSet generation does not match"
  type       = "infra_metric"
  event      = "K8sStatefulsetSample"
  select     = "observedGeneration - metadataGeneration"
  comparison = "above"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_stateful_set_not_been_rolled_out_crit" {
  policy_id = newrelic_alert_policy.kubernetes_critical.id

  name       = "StatefulSet update has not been rolled out"
  type       = "infra_metric"
  event      = "K8sStatefulsetSample"
  select     = "podsTotal - podsUpdated"
  comparison = "above"

  critical {
    duration      = 30
    value         = 0
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "kubernetes_alert_condition_stateful_set_not_been_rolled_out" {
  policy_id = newrelic_alert_policy.kubernetes_non_critical.id

  name       = "StatefulSet update has not been rolled out"
  type       = "infra_metric"
  event      = "K8sStatefulsetSample"
  select     = "podsTotal - podsUpdated"
  comparison = "above"

  critical {
    duration      = 15
    value         = 0
    time_function = "all"
  }
}