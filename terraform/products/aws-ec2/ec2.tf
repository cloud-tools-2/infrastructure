variable "instance_type" {
  description = "instance type"
  type        = string
}

variable "ami" {
  description = "ami"
  type        = string
}

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.name
  public_key = tls_private_key.private_key.public_key_openssh
}

resource "aws_instance" "ec2" {
  ami                     = var.ami
  instance_type           = var.instance_type
  key_name                = aws_key_pair.generated_key.key_name
  subnet_id               = data.aws_subnet.subnet.id
  vpc_security_group_ids  = [aws_security_group.sg.id]

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get --quiet update --yes",
      "sudo apt-get --quiet remove --yes docker docker-engine docker.io",
      "sudo apt --quiet install --yes docker.io",
      "sudo systemctl start docker"
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = aws_instance.ec2.public_ip
      private_key = tls_private_key.private_key.private_key_pem
    }
  }
}

output "private_key" {
  value = tls_private_key.private_key.private_key_pem
}

output "public_ip" {
  value = aws_instance.ec2.public_ip
}
