terraform {
  backend "s3" {}
  required_version = ">= 0.12.24"

  required_providers {
    aws      = ">= 2.55.0"
    local    = ">= 1.2"
    template = ">= 2.1"
  }
}

provider "aws" {
  region = var.aws_region
}

variable "aws_region" {
  description = "AWS Region"
  type        = string
}

variable "name" {
  description = "name"
  type        = string
}

variable "vpc_id" {
  description = "vpc_id"
  type        = string
}

data "aws_vpc" "vpc" {
  id = var.vpc_id
}

variable "subnet_id" {
  description = "subnet_id"
  type        = string
}

data "aws_subnet" "subnet" {
  id = var.subnet_id
}
