resource "aws_security_group" "sg" {
  name        = var.name
  description = "Security group for PostgreSQL"
  vpc_id      = data.aws_vpc.vpc.id
}

resource "aws_security_group_rule" "sg_rule_db" {
  description       = "Allow access to database port"
  type              = "ingress"
  security_group_id = aws_security_group.sg.id
  protocol          = "tcp"
  from_port         = 5432
  to_port           = 5432
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "sg_rule_internet" {
  description       = "Allow access to internet"
  type              = "egress"
  security_group_id = aws_security_group.sg.id
  protocol          = "tcp"
  from_port         = 0
  to_port           = 65535
  cidr_blocks       = ["0.0.0.0/0"]
}