resource "aws_db_instance" "db" {
  allocated_storage       = 20
  storage_type            = "gp2"
  engine                  = "postgres"
  engine_version          = "11.6"
  instance_class          = "db.t2.micro"
  name                    = var.name
  username                = "postgres"
  password                = "postgres"
  vpc_security_group_ids  = [aws_security_group.sg.id]
  publicly_accessible     = true
  skip_final_snapshot     = true
}

output "endpoint" {
  value = aws_db_instance.db.endpoint
}
