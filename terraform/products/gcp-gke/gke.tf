variable "machine_type" {
  description = "machine type"
  type        = string
}

variable "min_node_count" {
  description = "min node count"
  type        = number
}

variable "max_node_count" {
  description = "max node count"
  type        = number
}

variable "initial_node_count" {
  description = "initial node count"
  type        = number
}

variable "zones" {
  description = "zones"
  type        = list(string)
}

module "gke" {
  source                     = "../../vendor/terraform-google-kubernetes-engine"
  project_id                 = var.project_id
  name                       = var.name
  regional                   = false
  region                     = var.gcp_region
  zones                      = var.zones
  network                    = data.google_compute_network.network.name
  subnetwork                 = google_compute_subnetwork.subnetwork.name
  ip_range_pods              = google_compute_subnetwork.subnetwork.secondary_ip_range[0].range_name
  ip_range_services          = google_compute_subnetwork.subnetwork.secondary_ip_range[1].range_name
  http_load_balancing        = false
  horizontal_pod_autoscaling = false
  network_policy             = false
  create_service_account     = false
  skip_provisioners          = true

  node_pools = [
    {
      name               = var.name
      machine_type       = var.machine_type
      min_count          = var.min_node_count
      max_count          = var.max_node_count
      local_ssd_count    = 0
      disk_size_gb       = 32
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = var.initial_node_count
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {}

    default-node-pool = {
      default-node-pool = true
    }
  }

  node_pools_metadata = {
    all = {}

    default-node-pool = {
      node-pool-metadata-custom-value = "my-node-pool"
    }
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-pool",
    ]
  }
}
