data "google_compute_network" "network" {
  name = "default"
}

resource "google_compute_subnetwork" "subnetwork" {
  name                     = "subnetwork"
  region                   = var.gcp_region
  network                  = data.google_compute_network.network.name
  ip_cidr_range            = "10.0.36.0/24"
  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "pod"
    ip_cidr_range = "10.0.0.0/19"
  }

  secondary_ip_range {
    range_name    = "svc"
    ip_cidr_range = "10.0.32.0/22"
  }
}