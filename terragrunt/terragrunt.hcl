locals {
  dirs             = split("/", path_relative_to_include())
  org              = "cloud"
  product          = "infra"
  env              = local.dirs[0]
  stack            = local.dirs[1]
  aws_region       = "eu-west-1"
  state_aws_region = "eu-west-1"
  gcp_region       = "us-central1"

  secrets_path = "${get_terragrunt_dir()}/secrets.yaml"
  secrets      = yamldecode(fileexists(local.secrets_path) ? file(local.secrets_path) : "{}")
}

remote_state {
  backend = "s3"

  config = {
    encrypt = true
    bucket = join("-", [
      lower(local.org),
      lower(local.product),
      lower(local.env),
      lower(local.aws_region),
      "tfstate",
    ])
    key    = "${local.env}-${local.stack}.tfstate"
    region = local.state_aws_region
    dynamodb_table = join("-", [
      lower(local.org),
      lower(local.product),
      "tflock",
      lower(local.env),
    ])
  }
}
inputs = merge(
  local.secrets,
  {
    environment = local.env
    aws_region  = local.aws_region
    gcp_region  = local.gcp_region
  }
)
