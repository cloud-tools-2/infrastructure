include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform//products/aws-rds"
}

inputs = {
  name          = "rds-dev"
  vpc_id        = "vpc-a9fec5cf"
  subnet_id     = "subnet-04df1a1feb68d9603"
}
