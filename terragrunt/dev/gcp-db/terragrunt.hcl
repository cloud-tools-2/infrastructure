include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform//products/gcp-db"
}

inputs = {
  name                = "db-dev"
  project_id          = "praxis-zoo-278213"
  machine_type        = "db-f1-micro"
  zone                = "c"
  authorized_networks = [{
    name  = "my_own_ip"
    value = "83.31.178.33"
  }]
}
