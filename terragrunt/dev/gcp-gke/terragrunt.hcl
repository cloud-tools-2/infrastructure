include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform//products/gcp-gke"
}

inputs = {
  name               = "cluster-dev"
  project_id         = "praxis-zoo-278213"
  machine_type       = "n1-standard-2"
  min_node_count     = 1
  max_node_count     = 2
  initial_node_count = 1
  zones              = ["us-central1-c"]
}
