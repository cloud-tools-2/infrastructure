include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform//products/aws-ec2"
}

inputs = {
  name          = "ec2-dev"
  ami           = "ami-0701e7be9b2a77600"
  instance_type = "t2.micro"
  vpc_id        = "vpc-a9fec5cf"
  subnet_id     = "subnet-04df1a1feb68d9603"
}
